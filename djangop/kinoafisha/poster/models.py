from audioop import reverse
from distutils.command.upload import upload
from operator import truediv
from turtle import title
from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

class Poster(models.Model):
    title = models.CharField(max_length=255, verbose_name= "Заголовок")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    content = models.TextField(blank=True, verbose_name= "Жанр")
    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Фото" )
    is_published = models.BooleanField(default=True, verbose_name= "Публикация")
    cat = models.ForeignKey('Category', on_delete = models.PROTECT, verbose_name="Категории")
    history = HistoricalRecords()


    def get_absolute_url(self):
        return reverse('post', kwargs={'post_id': self.pk})

    def __str__(self):
        return self.title

   
        

    class Meta:
        verbose_name = 'Кино'
        verbose_name_plural = 'Кино'
        ordering = ['title']
        


class Category(models.Model):
    name = models.CharField(max_length=100, db_index=True, verbose_name="Категория")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    history = HistoricalRecords()


    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['id']



    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category', kwargs={'cat_id': self.pk})
        

class Reviews(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор отзыва", blank=True, null=True)
    data = models.DateTimeField('Дата публикации', auto_now=True)
    text = models.TextField('Текст отзыва')
    history = HistoricalRecords()


    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return f'/reviews/{self.id}'

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['-data']
    

class Comments(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор комментария", blank=True, null=True)
    data = models.DateTimeField('Дата комментария', auto_now=True)
    text = models.TextField('Текст комментария')
    history = HistoricalRecords()


    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return f'/reviews/{self.id}'

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['-data']
    

class News(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор новости", blank=True, null=True)
    data = models.DateTimeField('Дата новости', auto_now=True)
    text = models.TextField('Текст новости')
    actions = ['mark_as_published']
    history = HistoricalRecords()


    def mark_as_published(self, request, queryset):
        queryset.update(status="published")


    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return f'/reviews/{self.id}'

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-data']
    
class Series(models.Model):
    title = models.CharField(max_length=255, verbose_name= "Заголовок")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    content = models.TextField(blank=True, verbose_name= "Жанр")
    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Фото" )
    is_published = models.BooleanField(default=True, verbose_name= "Публикация")
    cat = models.ForeignKey('Category', on_delete = models.PROTECT, verbose_name="Категории")
    history = HistoricalRecords()



    def get_absolute_url(self):
        return reverse('post', kwargs={'post_id': self.pk})

    def __str__(self):
        return self.title

   
        

    class Meta:
        verbose_name = 'Сериал'
        verbose_name_plural = 'Сериалы'
        ordering = ['title']
        
