from django.contrib import admin
from .models import *
from django.utils.safestring import mark_safe
from django_object_actions import DjangoObjectActions, action
from simple_history.admin import SimpleHistoryAdmin
# Register your models here.

# Login root
# Password root

class PosterAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('id', 'title', 'get_html_photo', 'is_published')
    history_list_display = ["status"]
    list_display_links = ('id', 'title')
    search_fields = ('title', 'content')
    list_editable = ('is_published',)
    list_filter = ('is_published',)
    prepopulated_fields = {"slug": ("title",)}
    

    def get_html_photo(self, object):
        if object.photo:
            return mark_safe(f"<img src='{object.photo.url}' width=50>")

    get_html_photo.short_description = 'Фото'
    
class SeriesAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('id', 'title', 'get_html_photo', 'is_published')
    history_list_display = ["status"]
    list_display_links = ('id', 'title')
    search_fields = ('title', 'content')
    list_editable = ('is_published',)
    list_filter = ('is_published',)
    prepopulated_fields = {"slug": ("title",)}

    def get_html_photo(self, object):
        if object.photo:
            return mark_safe(f"<img src='{object.photo.url}' width=50>")

    get_html_photo.short_description = 'Фото'


    # def export_to_xls(self, request, obj):
    # query_record = Query.objects.all().values('profile__full_name')
    # return excel.make_response_from_records(
    #     query_record,
    #     'xlsx',
    #     file_name="Querys"
    # )

    # list_display = ('weight', 'height')
    # export_to_xls.label = "Export to Excel"
    # changelist_actions = ('export_to_xls',)
    


class CategoryAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    history_list_display = ["status"]
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')
    search_fields = ('name',)
    prepopulated_fields = {"slug": ("name",)}


class ReviewsAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    history_list_display = ["status"]
    list_display = ('id', 'text')
    list_display_links = ('id', 'text')
    search_fields = ('text',)

class CommentsAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    history_list_display = ["status"]
    list_display = ('id', 'text')
    list_display_links = ('id', 'text')
    search_fields = ('text',)


class NewsAdmin(SimpleHistoryAdmin, admin.ModelAdmin):
    history_list_display = ["status"]
    list_display = ('id', 'text')
    list_display_links = ('id', 'text')
    search_fields = ('text',)


admin.site.register(Poster, PosterAdmin,)
admin.site.register(Category, CategoryAdmin,)
admin.site.register(Reviews, ReviewsAdmin)
admin.site.register(Comments, CommentsAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Series, SeriesAdmin)



admin.site.site_title = 'Админ-панель сайта "Киноафиша"'
admin.site.site_header = 'Админ-панель сайта "Киноафиша"'

